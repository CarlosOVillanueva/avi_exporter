FROM golang:1.9 as build
WORKDIR /go/src/gitlab.com/CarlosOVilanueva/avi_exporter
ADD . .
RUN make build-linux

FROM alpine:3.6

EXPOSE 8080
RUN apk update && apk --no-cache add ca-certificates
COPY --from=build /go/src/gitlab.com/CarlosOVillanueva/avi_exporter/target/linux/avi_exporter /avi_exporter

ENTRYPOINT ["/avi_exporter"]
