# AVI_EXPORTER

## Description
AVI_EXPORTER is a Prometheus metric exporter written in GO.

## Functional Requirements
The application will:

- Execute from within a Docker container
- Accept the following Environmental arguments:
 - AVI_USERNAME *string* AVI cluster username
 - AVI_PASSWORD *string* AVI cluster password
 - AVI_CLUSTER *string* IP or Host Name of AVI cluster 
 - AVI_TENANT *string* AVI Tenant (default is 'admin')
- Implement AVI SDK and vendor supported methods to retrieve metrics
- Implement Prometheus (prom) Client library to convert retrieved metrics to prom metrics
- Write prom metrics to */metrics* web path
- Spawn HTTP service and expose */metrics* path

## Development Guidelines
- Usernames and Passwords cannot be committed in cleartext to GitLab. Use GitLab secure secrets or omit them altogether from the code.

## Usage
### From Container
#### Docker
```
docker run --name <NAME> -p <HOST PORT>:8080 \
-e AVI_USERNAME="<USERNAME>" \
-e AVI_PASSWORD='<PASSWORD>' \
-e AVI_TENANT="admin" \
-e AVI_CLUSTER="<IP OR HOSTNAME>" \
-dit gitlab.com/CarlosOVillanueva/avi_exporter:latest
```
### From Source
This process requires `make` and `go` libraries.

1. Install `go` and `make` dependencies if you don't already have them.
2. Navigate to your GO `src` directory. On linux systems, `cd "$GOPATH/src"`. If this does not work, `echo $GOPATH`. It should return the default path to where your GO assets are stored. If not, `export GOPATH=<path to your go assets>`. If that path does not exists, `mkdir -p /go/src` to create a new asset directory.
3. Download repo `git clone https://gitlab.com/CarlosOVillanueva/avi_exporter.git`
4. Run `cd $GOPATH/src/avi_exporter`
3. Run `make build-linux` to create Binary
4. Export required environmental variables:
 - `export AVI_USERNAME=<USERNAME>`
 - `export AVI_PASSWORD=<PASSWORD>`
 - `export AVI_CLUSTER=<IP OR HOSTNAME OF TARGET AVI SYSTEM>`
 - `export AVI_TENANT=admin`
5. To run the binary, from the `avi_exporter folder` run `target/linux/avi_exporter`
