package main

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// ClusterResponse extends models.
type ClusterResponse struct {
	NodeInfo struct {
		MgmtIP string `json:"mgmt_ip"`
	} `json:"node_info"`
}

// VirtualService extends models.
type VirtualService struct {
	Results []struct {
		UUID string `json:"uuid"`
		Name string `json:"name"`
	} `json:"results"`
}

// MetricRequest extends models.
type MetricRequest struct {
	Step           int    `json:"step"`
	Limit          int    `json:"limit"`
	EntityUUID     string `json:"entity_uuid"`
	MetricID       string `json:"metric_id"`
	IncludeName    string `json:"include_name"`
	IncludeRefs    string `json:"include_refs"`
	PadMissingData string `json:"pad_missing_data"`
}

// Metrics extends models.
type Metrics struct {
	MetricRequests []MetricRequest `json:"metric_requests"`
}

// Response extends models.
type Response struct {
	Header struct {
		Name      string `json:"name"`
		Units     string `json:"units"`
		EntityRef string `json:"entity_ref"`
	} `json:"header"`
	Data []struct {
		Timestamp time.Time `json:"timestamp"`
		Value     float64   `json:"value"`
	} `json:"data"`
}

// Connection extends models.
type Connection struct {
	UserName string
	Password string
	Host     string
	Tenant   string
}

// Gauge extends models.
type Gauge struct {
	Name   string
	Entity string
	Units  string
	Value  float64
	Tenant string
	Leader string
}

// Exporter extends models.
type Exporter struct {
	prometheus.Collector
	L7ServerMaxConcurrentSessions  *prometheus.Desc
	L7ServerAvgCompleteResponses   *prometheus.Desc
	L4ServerAvgCompleteConns       *prometheus.Desc
	L4ServerAvgHealthStatus        *prometheus.Desc
	L4ServerSumHealthCheckFailures *prometheus.Desc
	L7ClientAvgSslConnections      *prometheus.Desc
	L7ServerAvgTotalRequests       *prometheus.Desc
	L4ClientAvgTxBytes             *prometheus.Desc
	L4ClientAvgRxBytes             *prometheus.Desc
	L4ServerAvgPoolCompleteConns   *prometheus.Desc
	L4ServerAvgOpenConns           *prometheus.Desc
	L4ServerAvgNewEstablishedConns *prometheus.Desc
	L4ServerSumConnectionsDropped  *prometheus.Desc
	L4ServerSumFinishedConns       *prometheus.Desc
	L4ClientAvgCompleteConns       *prometheus.Desc
	L4ClientSumFinishedConns       *prometheus.Desc
	SEStatsAvgConnections          *prometheus.Desc
	SEStatsAvgConnectionsDroppea   *prometheus.Desc
}
