package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/avinetworks/sdk/go/clients"
	"github.com/avinetworks/sdk/go/session"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	// Environmental variables for Docker/Kubernetes configuration.
	envUserName = os.Getenv("AVI_USERNAME")
	envPassword = os.Getenv("AVI_PASSWORD")
	envHost     = os.Getenv("AVI_CLUSTER")
	envTenant   = os.Getenv("AVI_TENANT")

	// Metric variables for Prometheus.
	customLabels   = []string{"obj_name", "units", "tenant_ref", "leader"}
	constantLabels prometheus.Labels
	metrics        = "l7_server.max_concurrent_sessions,l7_server.avg_complete_responses,l4_server.avg_complete_conns,l4_server.avg_health_status,l4_server.sum_health_check_failures,l7_client.avg_ssl_connections,l7_server.avg_total_requests,l4_client.avg_tx_bytes,l4_client.avg_rx_bytes,l4_server.avg_pool_complete_conns,l4_server.avg_open_conns,l4_server.avg_new_established_conns,l4_server.sum_connections_dropped,l4_server.sum_finished_conns,l4_client.avg_complete_conns,l4_client.sum_finished_conns,se_stats.avg_connections,se_stats.avg_connections_dropped"
)

// Connect establishes connection to AVI cluster or target node.
func connect(c Connection) (*clients.AviClient, error) {
	// simplify avi connection
	aviClient, err := clients.NewAviClient(c.Host, c.UserName,
		session.SetPassword(c.Password),
		session.SetTenant(c.Tenant),
		session.SetInsecure)

	return aviClient, err
}

func getLeader(s *clients.AviClient) string {
	// get leader for connection metrics
	path := "/api/cluster/runtime"
	response := ClusterResponse{}
	s.AviSession.Get(path, &response)

	return response.NodeInfo.MgmtIP
}

func getVirtualService(s *clients.AviClient) map[string]string {
	// get virtual services map
	path := "/api/virtualservice"
	response := VirtualService{}

	s.AviSession.Get(path, &response)

	// set map of virtual services
	vs := make(map[string]string)
	for _, val := range response.Results {
		vs[val.UUID] = val.Name
	}

	return vs
}

func (e *Exporter) setGauge(g Gauge, ch chan<- prometheus.Metric) {
	labels := []string{g.Entity, g.Units, g.Tenant, g.Leader}

	switch g.Name {
	case "l7_server.max_concurrent_sessions":
		ch <- prometheus.MustNewConstMetric(e.L7ServerMaxConcurrentSessions, prometheus.GaugeValue, g.Value, labels...)
	case "l7_server.avg_complete_responses":
		ch <- prometheus.MustNewConstMetric(e.L7ServerAvgCompleteResponses, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.avg_complete_conns":
		ch <- prometheus.MustNewConstMetric(e.L4ServerAvgCompleteConns, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.avg_health_status":
		ch <- prometheus.MustNewConstMetric(e.L4ServerAvgHealthStatus, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.sum_health_check_failures":
		ch <- prometheus.MustNewConstMetric(e.L4ServerSumHealthCheckFailures, prometheus.GaugeValue, g.Value, labels...)
	case "l7_client.avg_ssl_connections":
		ch <- prometheus.MustNewConstMetric(e.L7ClientAvgSslConnections, prometheus.GaugeValue, g.Value, labels...)
	case "l7_server.avg_total_requests":
		ch <- prometheus.MustNewConstMetric(e.L7ServerAvgTotalRequests, prometheus.GaugeValue, g.Value, labels...)
	case "l4_client.avg_tx_bytes":
		ch <- prometheus.MustNewConstMetric(e.L4ClientAvgTxBytes, prometheus.GaugeValue, g.Value, labels...)
	case "l4_client.avg_rx_bytes":
		ch <- prometheus.MustNewConstMetric(e.L4ClientAvgRxBytes, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.avg_pool_complete_conns":
		ch <- prometheus.MustNewConstMetric(e.L4ServerAvgPoolCompleteConns, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.avg_open_conns":
		ch <- prometheus.MustNewConstMetric(e.L4ServerAvgOpenConns, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.avg_new_established_conns":
		ch <- prometheus.MustNewConstMetric(e.L4ServerAvgNewEstablishedConns, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.sum_connections_dropped":
		ch <- prometheus.MustNewConstMetric(e.L4ServerSumConnectionsDropped, prometheus.GaugeValue, g.Value, labels...)
	case "l4_server.sum_finished_conns":
		ch <- prometheus.MustNewConstMetric(e.L4ServerSumFinishedConns, prometheus.GaugeValue, g.Value, labels...)
	case "l4_client.avg_complete_conns":
		ch <- prometheus.MustNewConstMetric(e.L4ClientAvgCompleteConns, prometheus.GaugeValue, g.Value, labels...)
	case "l4_client.sum_finished_conns":
		ch <- prometheus.MustNewConstMetric(e.L4ClientSumFinishedConns, prometheus.GaugeValue, g.Value, labels...)
	case "se_stats.avg_connections":
		ch <- prometheus.MustNewConstMetric(e.SEStatsAvgConnections, prometheus.GaugeValue, g.Value, labels...)
	case "se_stats.avg_connections_droppea":
		ch <- prometheus.MustNewConstMetric(e.SEStatsAvgConnectionsDroppea, prometheus.GaugeValue, g.Value, labels...)
	default:
		return
	}
}

// NewExporter extends main by allowing the creation of new exporter objects.
func NewExporter() *Exporter {
	return &Exporter{
		L7ServerMaxConcurrentSessions: prometheus.NewDesc(
			"l7_server_max_concurrent_sessions",
			"AVI Metric",
			customLabels,
			constantLabels),
		L7ServerAvgCompleteResponses: prometheus.NewDesc(
			"l7_server_avg_complete_responses",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerAvgCompleteConns: prometheus.NewDesc(
			"l4_server_avg_complete_conns",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerAvgHealthStatus: prometheus.NewDesc(
			"l4_server_avg_health_status",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerSumHealthCheckFailures: prometheus.NewDesc(
			"l4_server_sum_health_check_failures",
			"AVI Metric",
			customLabels,
			constantLabels),
		L7ClientAvgSslConnections: prometheus.NewDesc(
			"l7_client_avg_ssl_connections",
			"AVI Metric",
			customLabels,
			constantLabels),
		L7ServerAvgTotalRequests: prometheus.NewDesc(
			"l7_server_avg_total_requests",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ClientAvgTxBytes: prometheus.NewDesc(
			"l4_client_avg_tx_bytes",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ClientAvgRxBytes: prometheus.NewDesc(
			"l4_client_avg_rx_bytes",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerAvgPoolCompleteConns: prometheus.NewDesc(
			"l4_server_avg_pool_complete_conns",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerAvgOpenConns: prometheus.NewDesc(
			"l4_server_avg_open_conns",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerAvgNewEstablishedConns: prometheus.NewDesc(
			"l4_server_avg_new_established_conns",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerSumConnectionsDropped: prometheus.NewDesc(
			"l4_server_sum_connections_dropped",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ServerSumFinishedConns: prometheus.NewDesc(
			"l4_server_sum_finished_conns",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ClientAvgCompleteConns: prometheus.NewDesc(
			"l4_client_avg_complete_conns",
			"AVI Metric",
			customLabels,
			constantLabels),
		L4ClientSumFinishedConns: prometheus.NewDesc(
			"l4_client_sum_finished_conns",
			"AVI Metric",
			customLabels,
			constantLabels),
		SEStatsAvgConnections: prometheus.NewDesc(
			"se_stats_avg_connections",
			"AVI Metric",
			customLabels,
			constantLabels),
		SEStatsAvgConnectionsDroppea: prometheus.NewDesc(
			"se_stats_avg_connections_droppea",
			"AVI Metric",
			customLabels,
			constantLabels),
	}
}

// Collect extends metrics from target endpoints.
func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	// Connect to cluster and return leader.
	clusterSessionArgs := Connection{UserName: envUserName, Password: envPassword, Tenant: envTenant, Host: envHost}
	clusterSession, err := connect(clusterSessionArgs)

	if err != nil {
		fmt.Printf("%s", err)
		return
	}

	// Get leader.
	leader := getLeader(clusterSession)

	// Connect to leader and establish AviSession.
	sessionArgs := Connection{UserName: envUserName, Password: envPassword, Tenant: envTenant, Host: leader}
	session, err := connect(sessionArgs)

	if err != nil {
		fmt.Printf("%s", err)
		return
	}

	// Set vsMap.
	vsMap := getVirtualService(session)

	// Get metrics.
	mr := MetricRequest{Step: 5, Limit: 1, EntityUUID: "*", MetricID: metrics, IncludeName: "True", IncludeRefs: "True", PadMissingData: "False"}
	sr := []MetricRequest{}
	sr = append(sr, mr)

	req := Metrics{MetricRequests: sr}
	path := "/api/analytics/metrics/collection"
	var rsp map[string]map[string][]Response

	session.AviSession.Post(path, req, &rsp)

	// Iterate through metrics and format results.
	for _, s := range rsp["series"] {
		for _, d := range s {
			// metric variables
			var entity string
			metric := d.Header.Name
			val := d.Data[0].Value
			units := d.Header.Units
			entityref := d.Header.EntityRef
			entityMap := strings.Split(entityref, "/")
			entityKey := entityMap[5]
			virtualService := vsMap[entityKey]

			if virtualService == "" {
				entity = entityref
			} else {
				entity = virtualService
			}
			//set prom gauge
			gauge := Gauge{Name: metric, Entity: entity, Value: val, Units: units, Tenant: envTenant, Leader: leader}
			e.setGauge(gauge, ch)
		}
	}
}

// Describe extends main by adding meta to the metrics.
func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- e.L7ServerMaxConcurrentSessions
	ch <- e.L7ServerAvgCompleteResponses
	ch <- e.L4ServerAvgCompleteConns
	ch <- e.L4ServerAvgHealthStatus
	ch <- e.L4ServerSumHealthCheckFailures
	ch <- e.L7ClientAvgSslConnections
	ch <- e.L7ServerAvgTotalRequests
	ch <- e.L4ClientAvgTxBytes
	ch <- e.L4ClientAvgRxBytes
	ch <- e.L4ServerAvgPoolCompleteConns
	ch <- e.L4ServerAvgOpenConns
	ch <- e.L4ServerAvgNewEstablishedConns
	ch <- e.L4ServerSumConnectionsDropped
	ch <- e.L4ServerSumFinishedConns
	ch <- e.L4ClientAvgCompleteConns
	ch <- e.L4ClientSumFinishedConns
	ch <- e.SEStatsAvgConnections
	ch <- e.SEStatsAvgConnectionsDroppea
}
